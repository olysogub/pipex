/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pipex_errors.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: djagu <djagu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/31 16:53:11 by djagu             #+#    #+#             */
/*   Updated: 2013/12/31 17:23:09 by djagu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"
#include "libft.h"
#include <unistd.h>
#include <errno.h>

void		ft_putopen_error(char *file_name)
{
	ft_putstr_fd(file_name, ERROR_FD);
	ft_putstr_fd(": ", ERROR_FD);
	if (errno == 2)
		ft_putstr_fd("No such file or directory", ERROR_FD);
	else if (errno == 13)
		ft_putstr_fd("Permission denied", ERROR_FD);
	else
		ft_putstr_fd("An error has occured", ERROR_FD);
	ft_putstr_fd("\n", ERROR_FD);
	exit(0);
}

void		ft_pipeerror(void)
{
		ft_putstr_fd("An error occured\n", 2);
		exit(0);
}