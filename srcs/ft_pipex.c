/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pipex.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: djagu <djagu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/30 14:56:10 by olysogub          #+#    #+#             */
/*   Updated: 2013/12/31 17:09:23 by djagu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"
#include "unistd.h"
#include <stdio.h>
#include "libft.h"
#include <fcntl.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>

void		ft_testexec(char *name, char **path, char **av)
{
	int			i;
	char		*p;
	char		*p2;

	i = -1;
	while (path[++i])
	{
		p = ft_strjoin(path[i], "/");
		p2 = ft_strjoin(p, name);
		free(p);
		if (access(p2, F_OK) == 0)
			break ;
		if (path[i + 1])
			free(p2);
	}
	execve(p2, av, NULL);
	free(p2);
	ft_putstr_fd("command not found: ", ERROR_FD);
	ft_putstr_fd(name, ERROR_FD);
	ft_putstr_fd("\n", ERROR_FD);
}

void		ft_findexec(char *name, char **av)
{
	extern char		**environ;
	int				i;
	char			*path;
	char			**epath;

	i = -1;
	path = NULL;
	while (environ[++i])
	{
		path = ft_strstr(environ[i], "PATH=");
		if (path && ((environ[i] - path) == 0))
			break ;
	}
	epath = ft_strsplit(path, ':');
	ft_testexec(name, epath, av);
}

void		ft_startchild(pid_t pid, char *file1, int *pfd)
{
	int		fd;
	int		ret;
	char	buff[BUFF_SIZE + 1];

	fd = open(file1, O_RDONLY);
	if (fd < 0)
		ft_putopen_error(file1);
	if (pid == 0)
	{
		close(pfd[0]);
		dup2(pfd[1], 1);
		while ((ret = read(fd, buff, BUFF_SIZE)) > 0)
			write(pfd[1], buff, ret);
		close(pfd[1]);
		close(fd);
		exit(0);
	}
}

void		ft_startchild2(pid_t *pid, int pfd[3][2], int fd, char *cmd2)
{
	int			ret;
	char		buff[BUFF_SIZE + 1];

	if ((pid[2] = fork()) == 0)
	{
		close(pfd[0][0]);
		close(pfd[0][1]);
		close(pfd[1][0]);
		close(pfd[1][1]);
		close(pfd[2][1]);
		while ((ret = read(pfd[2][0], buff, BUFF_SIZE)) > 0)
			write(fd, buff, ret);
	}
	else
	{
		close(pfd[0][0]);
		close(pfd[0][1]);
		close(pfd[2][0]);
		dup2(pfd[2][1], 1);
		close(pfd[2][1]);
		close(pfd[1][1]);
		dup2(pfd[1][0], 0);
		close(pfd[1][0]);
		ft_findexec(ft_strsplit(cmd2, ' ')[0], ft_strsplit(cmd2, ' '));
	}
}

void		ft_pipex(char *cmd1, char *file1, char *cmd2, char *file2)
{
	int		pfd[3][2];
	pid_t	pid[3];
	char	**param1;
	int		fd;

	fd = open(file2, O_RDWR|O_CREAT|O_TRUNC, S_IWUSR|S_IRUSR|S_IRGRP|S_IROTH);
	if (pipe(pfd[0]) < 0 || fd < 0 || pipe(pfd[1]) < 0 || pipe(pfd[2]) < 0)
		ft_pipeerror();
	pid[0] = fork();
	if (pid[0] == 0)
		ft_startchild(pid[0], file1, pfd[0]);
	pid[1] = fork();
	if (pid[1] == 0)
		ft_startchild2(pid, pfd, fd, cmd2);
	else
	{
		close(pfd[0][1]);
		dup2(pfd[0][0], 0);
		close(pfd[0][0]);
		close(pfd[1][0]);
		dup2(pfd[1][1], 1);
		close(pfd[1][1]);
		param1 = ft_strsplit(cmd1, ' ');
		ft_findexec(param1[0], param1);
	}
}
