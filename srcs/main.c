/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: olysogub <olysogub@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/30 13:26:11 by olysogub          #+#    #+#             */
/*   Updated: 2013/12/30 19:26:35 by olysogub         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "pipex.h"
#include <unistd.h>
#include "libft.h"

int				main(int ac, char **av)
{
	if (ac < 5)
	{
		ft_putstr_fd("Usage: ", ERROR_FD);
		ft_putstr_fd(av[0], ERROR_FD);
		ft_putstr_fd(" file1 cmd1 cmd2 file2\n", ERROR_FD);
		exit(0);
	}
	ft_pipex(av[2], av[1], av[3], av[4]);
	return (0);
}
