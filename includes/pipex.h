/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: djagu <djagu@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/30 13:38:34 by olysogub          #+#    #+#             */
/*   Updated: 2013/12/31 16:53:49 by djagu            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PIPEX_H
# define PIPEX_H
# define BUFF_SIZE 10
# define ERROR_FD 2

void		ft_pipex(char *cmd1, char *file1, char *cmd, char *file2);
void		ft_putopen_error(char *file_name);
void		ft_pipeerror();


#endif
