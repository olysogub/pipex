# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: djagu <djagu@student.42.fr>                +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/12/30 13:28:38 by olysogub          #+#    #+#              #
#    Updated: 2013/12/31 16:54:52 by djagu            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = pipex

HEADERS = -I includes/ -I libft/

LIBS = -L libft/ -lft 

GCC = gcc -Wall -Werror -Wextra

SRCS =	srcs/main.c \
		srcs/ft_pipex.c \
		srcs/ft_pipex_errors.c

OBJECTS = $(SRCS:.c=.o)

all: $(NAME)

makelib:
	make -C libft/

$(NAME): makelib $(OBJECTS)
	$(GCC) $(HEADERS) -o $(NAME) $(OBJECTS) $(LIBS)

.PHONY: fclean clean all re

%.o: %.c
	$(GCC) $(HEADERS) -o $@ -c $<

clean:
	rm -f $(OBJECTS)

fclean: clean
	rm -f $(NAME)

re: fclean all
